<?php

require_once dirname(__FILE__).'/../lib/vendor/symfony/lib/autoload/sfCoreAutoload.class.php';
sfCoreAutoload::register();

class ProjectConfiguration extends sfProjectConfiguration
{
  public function setup()
  {
    $this->enablePlugins('sfDoctrinePlugin');
    $this->enablePlugins('sfDoctrineGuardPlugin');
    $this->enablePlugins('sfForkedDoctrineApplyPlugin');
    $this->enablePlugins('sfFormExtraPlugin');
    $this->enablePlugins('sfPHPOpenIdPlugin');
    $this->enablePlugins('sfMyMasterPaypalPlugin');
    $this->enablePlugins('amgSentryPlugin');
    //$this->enablePlugins('sfErrorNotifierPlugin');
  }
}
