<?php

/**
 * order_recap actions.
 *
 * @package    SupLyon
 * @subpackage order_recap
 * @author     Your name here
 * @version    SVN: $Id: actions.class.php 23810 2009-11-12 11:07:44Z Kris.Wallsmith $
 */
class order_recapActions extends sfActions
{
 /**
  * Executes index action
  *
  * @param sfRequest $request A request object
  */
  public function executeIndex(sfWebRequest $request)
  {
  	$array_order=array();
  	$this->pizzas = Doctrine_Query::create()
  				->from('Pizza')
  	    	->execute();
  	foreach($this->pizzas as $pizza){
  		$array_order[$pizza->getId()]=array();
  		$ordered = $pizza->getOrdered();
  		foreach($ordered as $order){
  			if($order->getPayed()){
  				$trans = explode('-',$order->getTransaction());
  				$array_order[$pizza->getId()][$order->getId()]=$order->getMember()->getName().' ('.$order->getMember()->getIdbooster().') - '.$trans[0].' - '.$order->getEmailPaypal();
  			}
  		}
  	}
  	$this->orders = $array_order;
  }
}
