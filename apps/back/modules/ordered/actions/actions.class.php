<?php

require_once dirname(__FILE__).'/../lib/orderedGeneratorConfiguration.class.php';
require_once dirname(__FILE__).'/../lib/orderedGeneratorHelper.class.php';

/**
 * ordered actions.
 *
 * @package    SupLyon
 * @subpackage ordered
 * @author     Your name here
 * @version    SVN: $Id: actions.class.php 23810 2009-11-12 11:07:44Z Kris.Wallsmith $
 */
class orderedActions extends autoOrderedActions
{
}
