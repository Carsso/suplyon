<?php
class rememberFilter extends sfFilter
{
	public function execute($filterChain)
	{
		// Filters don't have direct access to the request and user objects.
		// You will need to use the context object to get them
		$request = $this->getContext()->getRequest();
		$user    = $this->getContext()->getUser();
		// Execute this filter only once
		if ($this->isFirstCall() && !$user->isAuthenticated() && ($user->getAttribute('openid_triedAutoLog') != 'yes'))
		{
			$user->setAttribute('openid_triedAutoLog', 'yes'); // Don't come back here anymore for this session
			$cookie = $request->getCookie('known_openid_identity');
			if (!empty($cookie)) {
				$user->setAttribute('openid_url', $cookie);
				return $this->getContext()->getController()->forward('authModule', 'autoSignin');
			}
		}
		// Execute next filter
		$filterChain->execute();
	}
}