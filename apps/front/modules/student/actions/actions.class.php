<?php

/**
 * student actions.
 *
 * @package    SupLyon
 * @subpackage student
 * @author     Your name here
 * @version    SVN: $Id: actions.class.php 23810 2009-11-12 11:07:44Z Kris.Wallsmith $
 */
class studentActions extends sfActions
{
 /**
  * Executes index action
  *
  * @param sfRequest $request A request object
  */
  public function executeIndex(sfWebRequest $request)
  {
    $this->students = Doctrine_Query::create()
    	->from('sfGuardUserProfile')
    	->orderBy('campusid, class, promotion, idbooster')
    	->execute();
  }
  
  public function generatePhoto($student, $force=false) {
    if(!file_exists($student->getUserPath().'/photo/original.jpg')
        OR !file_exists($student->getUserPath().'/photo/thumbnail.jpg')
        OR filemtime($student->getUserPath().'/photo/thumbnail.jpg') <= (time()-7*24*3600)
        OR (filemtime($student->getUserPath().'/photo/thumbnail.jpg') <= (time()-2*24*3600) && $force)
    ){
      //Grosse photo
	    $ch = curl_init($student->getCampusBoosterImageUrl());
      curl_setopt( $ch, CURLOPT_USERAGENT, "SupLyon.fr BOT (+http://suplyon.fr)" );
      curl_setopt( $ch, CURLOPT_CONNECTTIMEOUT, 2 );
      curl_setopt( $ch, CURLOPT_TIMEOUT, 2 );
      curl_setopt( $ch, CURLOPT_MAXREDIRS, 10 );
      curl_setopt( $ch, CURLOPT_ENCODING, 'gzip, deflate' );
      curl_setopt( $ch, CURLOPT_FOLLOWLOCATION, true );
      curl_setopt($ch, CURLOPT_VERBOSE, 1);
      curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
      curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, FALSE);
      curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
      curl_setopt( $ch, CURLOPT_AUTOREFERER, true );
	    $result = curl_exec($ch);
	    $httpCode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
	    curl_close($ch);
	    if($httpCode != 200) {
	    	$ch = curl_init('http://www.campus-booster.net/Booster/Skins/bleu/no_photoId.jpg');
        curl_setopt( $ch, CURLOPT_USERAGENT, "SupLyon.fr BOT (+http://suplyon.fr)" );
        curl_setopt( $ch, CURLOPT_CONNECTTIMEOUT, 2 );
        curl_setopt( $ch, CURLOPT_TIMEOUT, 2 );
        curl_setopt( $ch, CURLOPT_MAXREDIRS, 10 );
        curl_setopt( $ch, CURLOPT_ENCODING, 'gzip, deflate' );
        curl_setopt( $ch, CURLOPT_FOLLOWLOCATION, true );
        curl_setopt($ch, CURLOPT_VERBOSE, 1);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, FALSE);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt( $ch, CURLOPT_AUTOREFERER, true );
		    $result = curl_exec($ch);
	    	curl_close($ch);
	    }
	    file_put_contents($student->getUserPath().'/photo/original.jpg',$result);




      //Miniature
	    $n_largeur = 80;
	    $n_hauteur = 110;
	    
	    list($largeur, $hauteur, $img_type) = getimagesize($student->getUserPath().'/photo/original.jpg'); //list est un moyen plus pratique pour ne récupérer que ce qu'on veut
	    $rapport = $hauteur/$n_hauteur;
	    $tmp_largeur = $largeur/$rapport;
	    $tmp_hauteur = $n_hauteur;
	    if($tmp_largeur<$n_largeur){
	    	$tmp_largeur=$n_largeur;
	    }
	    
	    //on ouvre la source
      switch($img_type){
        case IMAGETYPE_GIF:
          $source = imagecreatefromgif($student->getUserPath().'/photo/original.jpg');
          break;
        case IMAGETYPE_JPEG:
          $source = imagecreatefromjpeg($student->getUserPath().'/photo/original.jpg');
          break;
        case IMAGETYPE_WBMP:
          $source = imagecreatefromwbmp($student->getUserPath().'/photo/original.jpg');
          break;
        case IMAGETYPE_PNG:
          $source = imagecreatefrompng($student->getUserPath().'/photo/original.jpg');
          break;
        case IMAGETYPE_BMP:
          $source = $this->imagecreatefrombmp($student->getUserPath().'/photo/original.jpg');
          break;
      }

      $temporary = imagecreatetruecolor($tmp_largeur, $tmp_hauteur);
	    imagecopyresampled($temporary, $source, 0, 0, 0, 0, $tmp_largeur, $tmp_hauteur, $largeur, $hauteur);
	    
	    $destination = imagecreatetruecolor($n_largeur, $n_hauteur);
	    imagecopy($destination, $temporary, 0, 0, $tmp_largeur/2-$n_largeur/2, 0, $n_largeur, $n_hauteur);
	    
	    imagejpeg($destination,$student->getUserPath().'/photo/thumbnail.jpg');
	    imagedestroy($source);
	    imagedestroy($temporary);
	    imagedestroy($destination);
    }
  }
  public function executePhoto(sfWebRequest $request) {    
  	$student = $this->getRoute()->getObject();
  	$force = $this->getRequest()->getParameter('force',false);
  	$this->generatePhoto($student, $force);
    $this->redirect('/uploads/'.$student->getUserDir().'/photo/thumbnail.jpg');
  }
  public function executeBigPhoto(sfWebRequest $request) {
  	$student = $this->getRoute()->getObject();
  	$force = $this->getRequest()->getParameter('force',false);
  	$this->generatePhoto($student, $force);
  	$this->redirect('/uploads/'.$student->getUserDir().'/photo/original.jpg');
  }
  public function imagecreatefrombmp($filename) {
      //Ouverture du fichier en mode binaire
      if (! $f1 = fopen($filename,"rb")) return FALSE;

      //1 : Chargement des ent�tes FICHIER
      $FILE = unpack("vfile_type/Vfile_size/Vreserved/Vbitmap_offset", fread($f1,14));
      if ($FILE['file_type'] != 19778) return FALSE;

      //2 : Chargement des ent�tes BMP
      $BMP = unpack('Vheader_size/Vwidth/Vheight/vplanes/vbits_per_pixel'.
          '/Vcompression/Vsize_bitmap/Vhoriz_resolution'.
          '/Vvert_resolution/Vcolors_used/Vcolors_important', fread($f1,40));
      $BMP['colors'] = pow(2,$BMP['bits_per_pixel']);
      if ($BMP['size_bitmap'] == 0) $BMP['size_bitmap'] = $FILE['file_size'] - $FILE['bitmap_offset'];
      $BMP['bytes_per_pixel'] = $BMP['bits_per_pixel']/8;
      $BMP['bytes_per_pixel2'] = ceil($BMP['bytes_per_pixel']);
      $BMP['decal'] = ($BMP['width']*$BMP['bytes_per_pixel']/4);
      $BMP['decal'] -= floor($BMP['width']*$BMP['bytes_per_pixel']/4);
      $BMP['decal'] = 4-(4*$BMP['decal']);
      if ($BMP['decal'] == 4) $BMP['decal'] = 0;

      //3 : Chargement des couleurs de la palette
      $PALETTE = array();
      if ($BMP['colors'] < 16777216)
      {
        $PALETTE = unpack('V'.$BMP['colors'], fread($f1,$BMP['colors']*4));
      }

      //4 : Cr�ation de l'image
      $IMG = fread($f1,$BMP['size_bitmap']);
      $VIDE = chr(0);

      $res = imagecreatetruecolor($BMP['width'],$BMP['height']);
      $P = 0;
      $Y = $BMP['height']-1;
      while ($Y >= 0)
      {
        $X=0;
        while ($X < $BMP['width'])
        {
          if ($BMP['bits_per_pixel'] == 24)
            $COLOR = unpack("V",substr($IMG,$P,3).$VIDE);
          elseif ($BMP['bits_per_pixel'] == 16)
          {
            $COLOR = unpack("n",substr($IMG,$P,2));
            $COLOR[1] = $PALETTE[$COLOR[1]+1];
          }
          elseif ($BMP['bits_per_pixel'] == 8)
          {
            $COLOR = unpack("n",$VIDE.substr($IMG,$P,1));
            $COLOR[1] = $PALETTE[$COLOR[1]+1];
          }
          elseif ($BMP['bits_per_pixel'] == 4)
          {
            $COLOR = unpack("n",$VIDE.substr($IMG,floor($P),1));
            if (($P*2)%2 == 0) $COLOR[1] = ($COLOR[1] >> 4) ; else $COLOR[1] = ($COLOR[1] & 0x0F);
            $COLOR[1] = $PALETTE[$COLOR[1]+1];
          }
          elseif ($BMP['bits_per_pixel'] == 1)
          {
            $COLOR = unpack("n",$VIDE.substr($IMG,floor($P),1));
            if     (($P*8)%8 == 0) $COLOR[1] =  $COLOR[1]        >>7;
            elseif (($P*8)%8 == 1) $COLOR[1] = ($COLOR[1] & 0x40)>>6;
            elseif (($P*8)%8 == 2) $COLOR[1] = ($COLOR[1] & 0x20)>>5;
            elseif (($P*8)%8 == 3) $COLOR[1] = ($COLOR[1] & 0x10)>>4;
            elseif (($P*8)%8 == 4) $COLOR[1] = ($COLOR[1] & 0x8)>>3;
            elseif (($P*8)%8 == 5) $COLOR[1] = ($COLOR[1] & 0x4)>>2;
            elseif (($P*8)%8 == 6) $COLOR[1] = ($COLOR[1] & 0x2)>>1;
            elseif (($P*8)%8 == 7) $COLOR[1] = ($COLOR[1] & 0x1);
            $COLOR[1] = $PALETTE[$COLOR[1]+1];
          }
          else
            return FALSE;
          imagesetpixel($res,$X,$Y,$COLOR[1]);
          $X++;
          $P += $BMP['bytes_per_pixel'];
        }
        $Y--;
        $P+=$BMP['decal'];
      }

      //Fermeture du fichier
      fclose($f1);

      return $res;
    }
}
