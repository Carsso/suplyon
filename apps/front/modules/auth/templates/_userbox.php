
        <?php if($sf_user->isAuthenticated()): ?>
        	<h2><?php echo $sf_user->getProfile()->getIdBooster() ?></h2>
	        <form action="<?php echo url_for('@sf_guard_signout') ?>" method="post">
		        <div id="userphoto">
	        		<?php echo image_tag(url_for('student_photo',$sf_user->getProfile()).'?force=true', array('style'=>'width:80px;', 'title'=>$sf_user->getProfile()->getName())) ?>
        		</div>
	        	<input name="connection" type="submit" value="Déconnexion" onclick="disconnectMini();" />
	        </form> 
        <?php else: ?>
        	<h2>Connexion</h2>
	        <form action="https://id.suplyon.fr/connexion/openid/" id="connexion_form" method="post">
		        <input name="idbooster" id="campusid" type="text" placeholder="Campus ID" />
		        <input type="submit" value="Se connecter" />
	        </form> 
	        
	        <script type="text/javascript">
	        	$('#connexion_form').submit(function(){
							if($('#campusid').val()==''){
								return false;
							}
              $('#connexion_form').attr('action',$('#connexion_form').attr('action')+$('#campusid').val());
		        });
	        </script>
        <?php endif; ?>