<?php
class authActions extends sfActions {
    public function executeIndex(sfWebRequest $request)
    {
      if($request->hasParameter('token')){
        $idbooster = $request->getParameter('idbooster');
        $token = $request->getParameter('token');
        $site_token = 'd435b66eaf03845a6cbfff5e4bbdbf9e';
        $url_api = 'https://id.suplyon.fr/user/getinfo/'.$site_token.'/'.$token;
        $ch = curl_init($url_api);
        curl_setopt( $ch, CURLOPT_USERAGENT, "SupLyon.fr BOT (+http://suplyon.fr)" );
        curl_setopt( $ch, CURLOPT_CONNECTTIMEOUT, 2 );
        curl_setopt( $ch, CURLOPT_TIMEOUT, 2 );
        curl_setopt( $ch, CURLOPT_MAXREDIRS, 10 );
        curl_setopt( $ch, CURLOPT_ENCODING, 'gzip, deflate' );
        curl_setopt( $ch, CURLOPT_FOLLOWLOCATION, true );
        curl_setopt($ch, CURLOPT_VERBOSE, 1);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, FALSE);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt( $ch, CURLOPT_AUTOREFERER, true );
        $result = curl_exec($ch);
        $httpCode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
        curl_close($ch);
        if($httpCode == 200) {
          $userinfo = json_decode($result, true);
          $user = Doctrine::getTable('sfGuardUserProfile')->findOneBy('idbooster',$userinfo['idbooster']);
          if(!is_object($user) OR !is_object($user->getUser())){
            $guarduser = new sfGuardUser();
            $guarduser->setEmailAddress($userinfo['email_address'])
                ->setUsername($userinfo['name'])
                ->setEmailAddress($userinfo['email_address'])
                ->setIsActive(1)
                ->save();
            $user = new Member();
            $user->setUserId($guarduser->getId())
                ->setPseudo($userinfo['idbooster']);
          } else {
            $guarduser = $user->getUser();
          }
          $user->setName($userinfo['name'])
              ->setIdbooster($userinfo['idbooster'])
              ->setCampusid($userinfo['campusid'])
              ->setCampus($userinfo['campus'])
              ->setClass($userinfo['class'])
              ->setPromotion($userinfo['promotion'])
              ->setCursus($userinfo['cursus'])
              ->setRole($userinfo['role'])
              ->setRank($userinfo['rank'])
              ->setEjabberdPassword(md5(sha1($userinfo['idbooster'].md5(uniqid()))))
              ->save();
          $this->getResponse()->setCookie('idbooster',$userinfo['idbooster']);
          $this->getUser()->signIn($guarduser, true);
          $this->redirect('@homepage');
        }
        $this->forward404('[ID.SupLyon.fr] Erreur de connexion !');
      }
    }

    public function executeFakeLogin(sfWebRequest $request)
    {
      if(sfConfig::get('sf_environment') == 'dev'){
        if($user = Doctrine::getTable('sfGuardUserProfile')->findOneBy('idbooster',$request->getParameter('idbooster'))){
          $guarduser = $user->getUser();
          if(!$this->getUser()->getAttribute('fakelogin',null)){
            if($request->getRemoteAddress() == '127.0.0.1' OR ($request->getParameter('admin_bypass', false) && $this->getUser()->isAuthenticated() && $this->getUser()->getGuardUser()->getIsSuperAdmin())){
                amgSentry::sendMessage('[Usurpation][En cours] Usurpation de l\'identité de '.$guarduser.' par '.$this->getUser()->getGuardUser().' en cours...');
                $this->getUser()->signIn($guarduser, true);
                $this->getUser()->setAttribute('fakelogin',$this->getUser()->getGuardUser());
                $back = '@homepage';
                $this->redirect($back);
            } else {
              $this->forward404('[Usurpation][Non autorisé] Accès autorisé uniquement en local ou bypass avec compte admin logué !');
            }
          } else {
            $this->forward404('[Usurpation][Déjà en cours] Usurpation déjà en cours !');
          }
        } else {
          $this->forward404('[Usurpation][Inexistant] Utilisateur "'.$request->getParameter('idbooster').'" inexistant.');
        }
      } else {
        $this->forward404('[Usurpation][Interdit en prod] Interdit en mode production !');
      }
    }
    
    public function executeSecure(sfWebRequest $request)
    {
    	if($this->getRequest()->hasParameter('error_secure_msg')){
    		$this->error_secure_msg = $this->getRequest()->getParameter('error_secure_msg',null);
    	}
    }
}
