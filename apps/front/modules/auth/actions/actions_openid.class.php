<?php
class authActions extends sfActions {
    public function executeOpenidError() {
      $this->error = $this->getUser()->getFlash('openid_error');
      amgSentry::sendMessage($this->error);
      $this->redirect('@homepage');
    }
    public function openIDCallback($openid_validation_result)
    {
    	$this->getUser()->setAuthenticated(true);
    	$userdata = array();
    	foreach($openid_validation_result['userData'] as $datakey => $data){
    		$userdata[$datakey] = $data[0];
	    	if(isset($data[1]))
	    		$userdata[$datakey] = $data[1];
	    	if(isset($data[2]))
	    		$userdata[$datakey] = $data[2];
    	}
    	$user = Doctrine::getTable('sfGuardUserProfile')->findOneBy('idbooster',$userdata['idbooster']);
    	if(!is_object($user) OR !is_object($user->getUser())){
    		$this->getResponse()->setCookie('idbooster',$userdata['idbooster']);
    		$guarduser = new sfGuardUser();
    		$guarduser->setEmailAddress($userdata['idbooster'].'@supinfo.com')
	    		->setUsername($userdata['fullname'])
	    		->setEmailAddress($userdata['idbooster'].'@supinfo.com')
	    		->setIsActive(1)
	    		->save();
    		$user = new Member();
    		$user->setUserId($guarduser->getId())
    			->setPseudo($userdata['idbooster']);
    	} else {
    		$guarduser = $user->getUser();
    	}
    	$user->setName($userdata['fullname'])
	    	->setIdbooster($userdata['idbooster'])
	    	->setCampusid($userdata['campusid'])
	    	->setCampus(html_entity_decode($userdata['campus'],ENT_COMPAT,'UTF-8'))
	    	->setClass($userdata['class'])
	    	->setPromotion($userdata['promotion'])
	    	->setCursus($userdata['cursus'].' '.$userdata['cursusdetail'])
	    	->setRole($userdata['role'])
	    	->setRank($userdata['rank'])
	    	->setEjabberdPassword(md5(sha1($userdata['idbooster'].md5(uniqid()))))
	    	->save();
    	$this->getUser()->signIn($guarduser, true);
    	$back = '@homepage';
    	$this->redirect($back);
    }
    
    public function executeIndex(sfWebRequest $request)
    {
    }

    public function executeLoginOpenId(sfWebRequest $request)
    {
      if($request->isMethod('post')){
        $this->getUser()->getAttributeHolder()->clear();
        $getRedirectHtmlResult = $this->getRedirectHtml('https://id.supinfo.com/me/'.$request->getParameter('idbooster'));
        if (!empty($getRedirectHtmlResult['url']) && !$request->isXmlHttpRequest())
          $this->redirect($getRedirectHtmlResult['url']);
      } else {
        $this->redirect('@homepage');
      }
    }

    public function executeFakeLogin(sfWebRequest $request)
    {
      if(sfConfig::get('sf_environment') == 'dev'){
        if($user = Doctrine::getTable('sfGuardUserProfile')->findOneBy('idbooster',$request->getParameter('idbooster'))){
          $guarduser = $user->getUser();
          if(!$this->getUser()->getAttribute('fakelogin',null)){
            if($request->getRemoteAddress() == '127.0.0.1' OR ($request->getParameter('admin_bypass', false) && $this->getUser()->isAuthenticated() && $this->getUser()->getGuardUser()->getIsSuperAdmin())){
                amgSentry::sendMessage('[Usurpation][En cours] Usurpation de l\'identité de '.$guarduser.' par '.$this->getUser()->getGuardUser().' en cours...');
                $this->getUser()->signIn($guarduser, true);
                $this->getUser()->setAttribute('fakelogin',$this->getUser()->getGuardUser());
                $back = '@homepage';
                $this->redirect($back);
            } else {
              $this->forward404('[Usurpation][Non autorisé] Accès autorisé uniquement en local ou bypass avec compte admin logué !');
            }
          } else {
            $this->forward404('[Usurpation][Déjà en cours] Usurpation déjà en cours !');
          }
        } else {
          $this->forward404('[Usurpation][Inexistant] Utilisateur "'.$request->getParameter('idbooster').'" inexistant.');
        }
      } else {
        $this->forward404('[Usurpation][Interdit en prod] Interdit en mode production !');
      }
    }
    
    public function executeSecure(sfWebRequest $request)
    {
    	if($this->getRequest()->hasParameter('error_secure_msg')){
    		$this->error_secure_msg = $this->getRequest()->getParameter('error_secure_msg',null);
    	}
    }
}
