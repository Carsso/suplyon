<?php

/**
 * store actions.
 *
 * @package    SupLyon
 * @subpackage store
 * @author     Your name here
 * @version    SVN: $Id: actions.class.php 23810 2009-11-12 11:07:44Z Kris.Wallsmith $
 */
class storeActions extends sfActions
{
  public function executeIndex(sfWebRequest $request)
  {
    $this->bypass = false;
    if(sfConfig::get('sf_environment') == 'dev')
      $this->bypass = $request->getParameter('admin_bypass', false);
    if($this->bypass)
      amgSentry::sendMessage('[Pizza][Bypass] Bypass de la vente de pizzas');
    if(ConfigTable::getConfig('open') != 1 && !$this->bypass)
      $this->forward($this->getModuleName(), 'closed');
    $this->pizzas = Doctrine_Core::getTable('Pizza')
        ->createQuery('a')
        ->execute();
    $this->ordered = Doctrine_Query::create()
        ->from('Ordered')
        ->where('idbooster = ?', $this->getUser()->getProfile()->getIdbooster())
        ->andWhere('payed = 1')
        ->execute();
    $this->payment = $request->getParameter('payment', false);
  }


  public function executeChoose(sfWebRequest $request)
  {
    $this->bypass = false;
    if(sfConfig::get('sf_environment') == 'dev')
      $this->bypass = $request->getParameter('admin_bypass', false);
    if($this->bypass)
      amgSentry::sendMessage('[Pizza][Bypass] Bypass de la vente de pizzas');
    if($this->getUser()->getProfile()->getCampusid() != 60383 && !$this->bypass){
      $this->getRequest()->setParameter('error_secure_msg', 'La commande de Pizzas n\'est possible que pour les membres du campus de SUPINFO Lyon.');
      amgSentry::sendMessage('[Pizza][Campus interdit] Accès à la vente de pizza despuis un campus interdit');
      $this->forward('auth', 'secure');
    }
    if(ConfigTable::getConfig('open') != 1 && !$this->bypass)
      $this->forward($this->getModuleName(), 'closed');
    $this->pizza = $this->getRoute()->getObject();
    if(!$this->pizza->getIsVisible())
      $this->forward404('[Pizza][Non Disponible] Tentative d\'accès à une pizza non disponible');
  }

  public function executePaiement(sfWebRequest $request)
  {
    $pizza = $this->getRoute()->getObject();
    if($request->getParameter('especes', false)){
      $ordered = new Ordered();
      $ordered->setPizzaId($pizza->getId())
          ->setMember($this->getUser()->getProfile())
          ->setTransaction('Espèces (en attente)')
          ->save();
      $this->redirect('@pizza?payment=waiting');
    }
    $paypal = new sfMyMasterPaypal('8.60', 'Paiement d\'une Pizza "'.$pizza->getName().'" - Prix: 8.60€');
    $paypal->setParameter('CUSTOM', $this->getUser()->getProfile()->getIdbooster().'-'.$pizza->getId());
    $paypal->expressCheckout();
  }


  public function executeClosed(sfWebRequest $request)
  {
  }
}
