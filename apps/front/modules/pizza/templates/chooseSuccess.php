<div class="article" style="margin-bottom:10px;">
    <div class="article_title"><h3>Pizza choisie</h3></div>
    <div class="article_content">
        <div style="float:left; margin: 15px 5px 10px">
            <img alt="<?php echo $pizza->getName() ?>"
                 src="http://www.dominos.fr/media/img/cartes/pizzas/images/<?php echo $pizza->getDominosId() ?>_light.png"><br/>
        </div>
        <div style="float:left; width: 430px; margin: 10px 5px; text-align:justify">
            <?php echo $pizza->getName() ?><?php echo ($pizza->getIsSpecial() && $pizza->getIsVisible()) ? '<span style="color:red; font-size:12px; margin-left:5px;">Édition Limitée !</span>' : '' ?>
            <br/>
            <span style="font-size: 13px;"><?php echo $pizza->getIngredients() ?></span>
        </div>
        <div style="float:right; width: 60px; text-align:center">
            <iframe
                src="http://www.facebook.com/plugins/like.php?locale=fr_FR&href=<?php echo urlencode(url_for('pizza_item', $pizza, true)) ?>&layout=box_count&show_faces=false&width=60&action=like&colorscheme=light&height=62"
                scrolling="no" frameborder="0"
                style="margin-top:5px; border:none; overflow:hidden; height: 62px; width: 60px;"
                allowtransparency="true"></iframe>
        </div>
        <div style="float:right; width: 90px; margin: 10px 5px;text-align:center;">
            Prix public:<br>
            <?php echo money_format('%!n &euro;', $pizza->getOriginalPrice()) ?>
        </div>
        <div style="clear:both;"></div>
    </div>
</div>
<?php
$is_mardi = (date('w') == 2) ? true : false;
$is_11h = (date('H') == 11 && date('i') < 51) ? true : false;
$is_more_9h = (date('H') > 8 && date('H') < 11) ? true : false;
?>
<?php if (($is_mardi && ($is_more_9h OR $is_11h)) OR $bypass): ?>
    <div class="article">
        <div class="article_title"><h3>Moyen de paiement</h3></div>
        <div class="article_content">
            <p>
                Choisissez votre moyen de paiement (paiement obligatoirement avant 11h50):
            </p>

            <div
                style="position:relative;width:325px;float:left;text-align:center;padding-left:10px; padding-right:10px;">
                <a href="<?php echo url_for('pizza_pre_paiement', $pizza) ?>">
                    <img src="https://www.paypalobjects.com/en_US/FR/i/bnr/bnr_horizontal_solution_PP_327wx80h.gif"
                         alt="Paiement par Paypal" style="margin-right:7px;">
                </a>
                Prix SupLyon: <?php echo money_format('%!n &euro;', $pizza->getPrice() + 0.6) //with paypal comission ?>
            </div>
            <?php
            // Regelement ESPACES DESACTIVE
            /*
            <div
                style="width:325xpx; float:right;text-align:center;padding-left:10px; padding-right:10px; padding-top:20px; border-left:1px solid #ccc">
                <input type="button" class="espece_payment" value="Paiement en Espèces" onclick=""
                       style="margin-bottom: 5px;"><br/>
                <span style="color:red;">NOUS NE RENDONS PAS LA MONNAIE</span><br/><br/>
                Prix SupLyon: <?php echo money_format('%!n &euro;', $pizza->getPrice() + 1) //with paypal comission ?>
            </div>
            <script type="text/javascript">
                $('.espece_payment').click(function () {
                    if (confirm('ATTENTION: Le paiement en espèces doit être effectué avant 11H50 afin de confirmer la commande.')) {
                        $(window).attr('location', '<?php echo url_for('pizza_paiement', $pizza).'?especes=true' ?>');
                    }
                });
            </script>
            */
            ?>
            <div style="clear:both;"></div>
        </div>
    </div>
<?php else: ?>
    <div class="article">
        <div class="article_title"><h3>Commande Impossible</h3></div>
        <p>Désolé, il n'est pas possible de commander actuellement.<br/>
            Les commandes doivent être passées le mardi, impérativement avant 11h50.</p>
    </div>
<?php endif; ?>


<?php
if ($bypass) {
    ?>
    <script type="text/javascript">
        $(document).ready(function () {
            newNotification('error', 'ATTENTION: La commande de Bypass Administrateur est actuellement activée...');
        });
    </script>
<?php
}
?>
		