<?php if($payment == 'ok'): ?>
	<div class="article">
		<div class="article_title"><h3>Paiement</h3></div>
		<p>
			Votre paiement a bien été validé, merci.
		</p>
	</div>
<?php endif; ?>
<?php if($payment == 'error'): ?>
	<div class="article">
		<div class="article_title"><h3>Paiement</h3></div>
		<p>
			Erreur, votre paiement n'a pas été validé.
		</p>
	</div>
<?php endif; ?>
<?php if($payment == 'waiting'): ?>
	<div class="article">
		<div class="article_title"><h3>Paiement</h3></div>
		<p>
			Merci de venir nous remettre votre paiement en espèces (B2). Attention, il doit être effectué avant 11H50.
		</p>
	</div>
<?php endif; ?>

<?php if(count($ordered)): ?>
	<div class="article">
		<div class="article_title"><h3>Pizzas payées</h3></div>
		<p>
			<?php foreach ($ordered as $order): ?>
				<img alt="<?php echo $order->getPizza()->getName() ?>" style="float:left;margin:5px;" title="<?php echo $order->getPizza()->getName() ?>" src="http://www.dominos.fr/media/img/cartes/pizzas/images/<?php echo $order->getPizza()->getDominosId() ?>_light.png">
			<?php endforeach; ?>
		</p>
	</div>
<div style="clear:both;"></div>
<?php endif; ?>
 
	<div class="article">
		<div class="article_title"><h3>Carte des Pizzas</h3></div>
		<div class="article_content">
			<?php foreach ($pizzas as $pizza): ?>
        <div style="float:left; margin: 15px 5px 10px">
          <img alt="<?php echo $pizza->getName() ?>" src="http://www.dominos.fr/media/img/cartes/pizzas/images/<?php echo $pizza->getDominosId() ?>_light.png">
        </div>
        <div style="float:left; width: 490px; margin: 10px 5px; text-align:justify">
              <?php echo $pizza->getName() ?><?php echo ($pizza->getIsSpecial() && $pizza->getIsVisible())?'<span style="color:red; font-size:12px; margin-left:5px;">Édition Limitée !</span>':'' ?><br />
            <span style="font-size: 13px;"><?php echo $pizza->getIngredients() ?></span>
        </div>
        <div style="float:right; margin: 10px 0 10px 5px;">
          <?php if($pizza->getIsVisible()): ?>
            <?php if($pizza->getIsSpecial()): ?>
              <a class="buttonify" style="background-color:#ba0f30" href="<?php echo url_for('pizza_choose', $pizza) ?>">Choisir</a>
            <?php else: ?>
              <a class="buttonify" href="<?php echo url_for('pizza_choose', $pizza) ?>">Choisir</a>
            <?php endif; ?>
          <?php else: ?>
            <span class="buttonify" style="background-color:#a3a3a3">Épuisé</span>
          <?php endif; ?>
          <iframe src="http://www.facebook.com/plugins/like.php?locale=fr_FR&href=<?php echo urlencode(url_for('pizza_item',$pizza,true)) ?>&layout=button_count&show_faces=false&width=90&action=like&colorscheme=light&height=21" scrolling="no" frameborder="0" style="margin-top:5px; border:none; overflow:hidden; width:90px; height:21px;" allowtransparency="true"></iframe>
        </div>
        <div style="clear:both;width:100%; border-bottom:1px solid #ccc"></div>
			<?php endforeach; ?>
		</div>
	</div>