
<div class="article">
  <div class="article_title"><h3>Redirection vers Paypal en cours...</h3></div>
  <p>
    Merci de bien vouloir patienter quelques instants...
  </p>
  <p style="text-align:center;">
    <?php echo image_tag('redirect_load.gif') ?>
  </p>
  <p style="font-size:10px;">
    Si rien ne se passe <?php echo link_to('cliquez ici','pizza_paiement',$pizza, array('style'=>'color:#666')) ?>
  </p>
</div>