

<div class="article">
  <div class="article_title"><h3>Pizza <?php echo $pizza->getName() ?> - En partenariat avec Domino's Pizza</h3></div>
  <div class="article_content">
    <div style="float:left; margin: 15px 5px 10px">
      <img alt="<?php echo $pizza->getName() ?>" src="http://www.dominos.fr/media/img/cartes/pizzas/images/<?php echo $pizza->getDominosId() ?>_light.png">
    </div>
    <div style="float:left; width: 460px; margin: 10px 5px; text-align:justify">
      <?php echo $pizza->getName() ?><?php echo ($pizza->getIsSpecial() && $pizza->getIsVisible())?'<span style="color:red; font-size:12px; margin-left:5px;">Édition Limitée !</span>':'' ?><br />
      <span style="font-size: 13px;"><?php echo $pizza->getIngredients() ?></span>
    </div>
    <div style="float:right; width: 120px; margin: 10px 5px;text-align:center;">
      Prix public:<br>
      <?php echo money_format('%!n &euro;',$pizza->getOriginalPrice()) ?><br />
      Prix SupLyon:<br />
      <?php echo money_format('%!n &euro;',$pizza->getPrice()+0.6) //with paypal comission ?>
    </div>
    <div style="clear:both;"></div>
  </div>
</div>

<?php slot('fb_metas') ?>
  <meta property="og:type" content="suplyon:pizza" />
  <meta property="og:title" content="Pizza <?php echo $pizza->getName() ?>" />
  <meta property="og:image" content="http://www.dominos.fr/media/img/cartes/pizzas/images/<?php echo $pizza->getDominosId() ?>_light.png" />
  <meta property="og:description" content="<?php echo $pizza->getIngredients() ?>" />
  <meta property="og:url" content="<?php echo url_for('pizza_item',$pizza,true) ?>">
<?php end_slot(); ?>
<!--<input type="button" onclick="postBuy()" />
<fb:add-to-timeline></fb:add-to-timeline>
<script type="text/javascript">
  function postBuy()
  {
    FB.api(
        '/me/suplyon:buy?pizza=<?php echo url_for('pizza_item',$pizza,true) ?>',
        'post',
        function(response) {
          if (!response || response.error) {
            alert('Error occured');
          } else {
            alert('Buy was successful! Action ID: ' + response.id);
          }
        });
  }
</script>-->