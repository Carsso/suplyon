<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
    "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
  <?php include_http_metas() ?>
  <?php include_metas() ?>
  <?php include_title() ?>
  <link rel="shortcut icon" href="/favicon.png"/>
  <?php include_stylesheets() ?>

  <meta property="fb:app_id" content="275910349150823" />
  <?php if (!include_slot('fb_metas')): ?>
    <meta property="fb:admins" content="1544443893" />
    <meta property="og:site_name" content="SupLyon" />
    <meta property="og:title" content="SupLyon" />
    <meta property="og:type" content="website" />
    <meta property="og:url" content="https://suplyon.fr" />
    <meta property="og:image" content="https://suplyon.fr/images/suplyon.png" />
  <?php endif; ?>

  <script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.6.4/jquery.min.js"></script>
  <script type="text/javascript" src="https://static.jappix.com/php/get.php?l=fr&amp;t=js&amp;g=mini.xml"></script>
  <?php include_javascripts() ?>

  <script type="text/javascript">

    var _gaq = _gaq || [];
    _gaq.push(['_setAccount', 'UA-21065019-1']);
    _gaq.push(['_setDomainName', 'suplyon.fr']);
    _gaq.push(['_trackPageview']);

    (function () {
      var ga = document.createElement('script');
      ga.type = 'text/javascript';
      ga.async = true;
      ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
      var s = document.getElementsByTagName('script')[0];
      s.parentNode.insertBefore(ga, s);
    })();

  </script>
  <?php if ($sf_user->isAuthenticated() && sfConfig::get('sf_environment') !== 'dev'): ?>
    <?php
    $city = $sf_user->getProfile()->getLowerCity();
    $class = strtolower($sf_user->getProfile()->getClass());
    $promo = strtolower($sf_user->getProfile()->getPromotion());
    ?>
    <script type="text/javascript">
      jQuery(document).ready(function () {
        MINI_GROUPCHATS = ["SupLyon@conference.suplyon.fr"];
        MINI_PASSWORDS = [];
        MINI_ANIMATE = true;
        MINI_RESOURCE = "Suplyon.fr";
        launchMini(true, true, "suplyon.fr", "<?php echo $sf_user->getProfile()->getSlug() ?>", "<?php echo $sf_user->getProfile()->getEjabberdPassword() ?>");
      });
    </script>
  <?php endif; ?>
</head>
<body>
<div id="fb-root"></div>
<script type="text/javascript">
  window.fbAsyncInit = function() {
    FB.init({
      appId      : '275910349150823', // App ID
      status     : true, // check login status
      cookie     : true, // enable cookies to allow the server to access the session
      xfbml      : true  // parse XFBML
    });
  };

  // Load the SDK Asynchronously
  (function(d){
    var js, id = 'facebook-jssdk'; if (d.getElementById(id)) {return;}
    js = d.createElement('script'); js.id = id; js.async = true;
    js.src = "//connect.facebook.net/en_US/all.js";
    d.getElementsByTagName('head')[0].appendChild(js);
  }(document));
</script>
<div id="messages" style="display: none;"></div>
<?php
if ($sf_user->getAttribute('fakelogin', false) && $sf_user->isAuthenticated()) {
  ?>
<script type="text/javascript">
  $(document).ready(function () {
    newNotification('error', 'ATTENTION: Usurpation de l\'identité de <?php echo $sf_user->getProfile() ?> par <?php echo $sf_user->getAttribute('fakelogin') ?> en cours...');
  });
</script>
  <?php
}
?>
<div id="header">
  <div id="header_content">
    <?php echo link_to(image_tag('suplyon.png'), '@homepage') ?>
  </div>
</div>

<div id="main">

  <div id="left_column">
    <div id="corps">

      <div id="page_title"><h2>Accueil</h2></div>

      <?php echo $sf_content ?>

    </div>

    <div id="footer">
      <p>Made by <a href="http://germain-carre.fr">Germain Carré</a> - Designed by <a href="http://maxime-steinhausser.fr">Maxime Steinhausser</a></p>
    </div>
  </div>

  <div id="right_column">

    <div id="connexion" class="right_bloc">
      <?php include_component('auth', 'userbox') ?>
    </div>

    <?php if ($sf_user->isAuthenticated()): ?>
    <div id="menu" class="right_bloc">
      <h2 class="header_right_bloc">A la carte</h2>
      <ul class="ul_menu">
        <li><a href="<?php echo url_for('@homepage') ?>">Accueil</a></li>
        <li><a href="<?php echo url_for('@pizza') ?>">Pizzas</a></li>
        <!--
                    <li><a href="">Minecraft</a></li>
                    <li><a href="">CSS</a></li>
                    <li><a href="">Mumble</a></li>
                    <li><a href="">Sondages</a></li>
                    <li><a href="">Documents</a></li>
                    <li><a href="">Evènements</a></li>
                    <li><a href="">Entraide</a></li>
                     -->
        <li><a href="<?php echo url_for('@membres') ?>">Membres</a></li>
        <?php if ($sf_user->isSuperAdmin()): ?>
        <li><a href="/back.php" target="_blank">Admin</a></li>
        <?php endif; ?>
      </ul>

    </div>

    <!--
              <div id="bloc_last_news" class="right_bloc">
              <h2 class="header_right_bloc">Last news</h2>
              <ul class="ul_menu3">
                <li><a href="" >News 1</a></li>
                <li><a href="" >News 2</a></li>
                <li><a href="" >News 3</a></li>
                <li><a href="" >News 4</a></li>
              </ul>
              </div>
              -->


    <?php endif; ?>
    <div id="facebook_page" class="right_bloc">
      <div class="fb-like-box" data-href="http://www.facebook.com/SupLyon" data-width="180" data-height="80" data-colorscheme="dark" data-show-faces="false" data-stream="false" data-header="false"></div>
    </div>
    <div id="bloc_autres" class="right_bloc">
      <h2 class="header_right_bloc">Autres</h2>
      <ul class="ul_menu">
        <li><a href="http://www.supinfo.com">Supinfo</a></li>
        <li><a href="http://www.campus-booster.net">Campus Booster</a></li>
        <li><a href="http://courses.supinfo.com">Courses</a></li>
        <li><a href="http://libraries.supinfo.com">Libraries</a></li>
        <li><a href="http://spr.supinfo.com">SPR</a></li>
        <li><a href="http://sngt.ubi-team.net">SNGT</a></li>
        <li><a href="http://yurplan.com">YurPlan</a></li>
      </ul>
    </div>

    <div id="social" class="right_bloc">
      <h2>Social</h2>
      <ul class="ul_menu2">
        <li><a id="facebook_icon" href="https://www.facebook.com/SupLyon">Facebook</a></li>
        <li><a id="twitter_icon" href="http://twitter.com/SupLyon">Twitter</a></li>
        <!--
                    <li><a id="rss_icon" href="#">RSS</a></li>
                    -->
        <li><a id="contact_icon" href="mailto:contact@suplyon.fr">Contact</a></li>
      </ul>
    </div>

    <div id="twitts" class="right_bloc">
      <script charset="utf-8" src="https://widgets.twimg.com/j/2/widget.js"></script>
      <script>
        new TWTR.Widget({
          version:2,
          type:'profile',
          rpp:2,
          interval:30000,
          width:180,
          height:250,
          theme:{
            shell:{
              background:'#454d5a',
              color:'#ffffff'
            },
            tweets:{
              background:'#f5f5f5',
              color:'#666666',
              links:'#00A5F0'
            }
          },
          features:{
            scrollbar:false,
            loop:false,
            live:true,
            behavior:'all'
          }
        }).render().setUser('SupLyon').start();
      </script>
    </div>
  </div>

</div>


</body>
</html>
