<?php

/**
 * Order filter form base class.
 *
 * @package    SupLyon
 * @subpackage filter
 * @author     Your name here
 * @version    SVN: $Id: sfDoctrineFormFilterGeneratedTemplate.php 29570 2010-05-21 14:49:47Z Kris.Wallsmith $
 */
abstract class BaseOrderFormFilter extends BaseFormFilterDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'idbooster'       => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('Member'), 'add_empty' => true)),
      'is_payed'        => new sfWidgetFormChoice(array('choices' => array('' => 'yes or no', 1 => 'yes', 0 => 'no'))),
      'payment_type'    => new sfWidgetFormFilterInput(array('with_empty' => false)),
      'transaction_ref' => new sfWidgetFormFilterInput(),
      'email_paypal'    => new sfWidgetFormFilterInput(),
      'status'          => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('OrderStatuses'), 'add_empty' => true)),
      'shipping_price'  => new sfWidgetFormFilterInput(),
      'extra_price'     => new sfWidgetFormFilterInput(),
      'created_at'      => new sfWidgetFormFilterDate(array('from_date' => new sfWidgetFormDate(), 'to_date' => new sfWidgetFormDate(), 'with_empty' => false)),
      'updated_at'      => new sfWidgetFormFilterDate(array('from_date' => new sfWidgetFormDate(), 'to_date' => new sfWidgetFormDate(), 'with_empty' => false)),
    ));

    $this->setValidators(array(
      'idbooster'       => new sfValidatorDoctrineChoice(array('required' => false, 'model' => $this->getRelatedModelName('Member'), 'column' => 'idbooster')),
      'is_payed'        => new sfValidatorChoice(array('required' => false, 'choices' => array('', 1, 0))),
      'payment_type'    => new sfValidatorPass(array('required' => false)),
      'transaction_ref' => new sfValidatorPass(array('required' => false)),
      'email_paypal'    => new sfValidatorPass(array('required' => false)),
      'status'          => new sfValidatorDoctrineChoice(array('required' => false, 'model' => $this->getRelatedModelName('OrderStatuses'), 'column' => 'id')),
      'shipping_price'  => new sfValidatorPass(array('required' => false)),
      'extra_price'     => new sfValidatorPass(array('required' => false)),
      'created_at'      => new sfValidatorDateRange(array('required' => false, 'from_date' => new sfValidatorDateTime(array('required' => false, 'datetime_output' => 'Y-m-d 00:00:00')), 'to_date' => new sfValidatorDateTime(array('required' => false, 'datetime_output' => 'Y-m-d 23:59:59')))),
      'updated_at'      => new sfValidatorDateRange(array('required' => false, 'from_date' => new sfValidatorDateTime(array('required' => false, 'datetime_output' => 'Y-m-d 00:00:00')), 'to_date' => new sfValidatorDateTime(array('required' => false, 'datetime_output' => 'Y-m-d 23:59:59')))),
    ));

    $this->widgetSchema->setNameFormat('order_filters[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    $this->setupInheritance();

    parent::setup();
  }

  public function getModelName()
  {
    return 'Order';
  }

  public function getFields()
  {
    return array(
      'id'              => 'Number',
      'idbooster'       => 'ForeignKey',
      'is_payed'        => 'Boolean',
      'payment_type'    => 'Text',
      'transaction_ref' => 'Text',
      'email_paypal'    => 'Text',
      'status'          => 'ForeignKey',
      'shipping_price'  => 'Text',
      'extra_price'     => 'Text',
      'created_at'      => 'Date',
      'updated_at'      => 'Date',
    );
  }
}
