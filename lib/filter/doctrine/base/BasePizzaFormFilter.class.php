<?php

/**
 * Pizza filter form base class.
 *
 * @package    SupLyon
 * @subpackage filter
 * @author     Your name here
 * @version    SVN: $Id: sfDoctrineFormFilterGeneratedTemplate.php 29570 2010-05-21 14:49:47Z Kris.Wallsmith $
 */
abstract class BasePizzaFormFilter extends BaseFormFilterDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'name'           => new sfWidgetFormFilterInput(array('with_empty' => false)),
      'ingredients'    => new sfWidgetFormFilterInput(array('with_empty' => false)),
      'dominos_id'     => new sfWidgetFormFilterInput(),
      'price'          => new sfWidgetFormFilterInput(),
      'original_price' => new sfWidgetFormFilterInput(),
      'is_visible'     => new sfWidgetFormChoice(array('choices' => array('' => 'yes or no', 1 => 'yes', 0 => 'no'))),
      'is_special'     => new sfWidgetFormChoice(array('choices' => array('' => 'yes or no', 1 => 'yes', 0 => 'no'))),
      'slug'           => new sfWidgetFormFilterInput(),
    ));

    $this->setValidators(array(
      'name'           => new sfValidatorPass(array('required' => false)),
      'ingredients'    => new sfValidatorPass(array('required' => false)),
      'dominos_id'     => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
      'price'          => new sfValidatorPass(array('required' => false)),
      'original_price' => new sfValidatorPass(array('required' => false)),
      'is_visible'     => new sfValidatorChoice(array('required' => false, 'choices' => array('', 1, 0))),
      'is_special'     => new sfValidatorChoice(array('required' => false, 'choices' => array('', 1, 0))),
      'slug'           => new sfValidatorPass(array('required' => false)),
    ));

    $this->widgetSchema->setNameFormat('pizza_filters[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    $this->setupInheritance();

    parent::setup();
  }

  public function getModelName()
  {
    return 'Pizza';
  }

  public function getFields()
  {
    return array(
      'id'             => 'Number',
      'name'           => 'Text',
      'ingredients'    => 'Text',
      'dominos_id'     => 'Number',
      'price'          => 'Text',
      'original_price' => 'Text',
      'is_visible'     => 'Boolean',
      'is_special'     => 'Boolean',
      'slug'           => 'Text',
    );
  }
}
