<?php

/**
 * Server filter form base class.
 *
 * @package    SupLyon
 * @subpackage filter
 * @author     Your name here
 * @version    SVN: $Id: sfDoctrineFormFilterGeneratedTemplate.php 29570 2010-05-21 14:49:47Z Kris.Wallsmith $
 */
abstract class BaseServerFormFilter extends BaseFormFilterDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'name'         => new sfWidgetFormFilterInput(array('with_empty' => false)),
      'image'        => new sfWidgetFormFilterInput(array('with_empty' => false)),
      'subdomain'    => new sfWidgetFormFilterInput(array('with_empty' => false)),
      'informations' => new sfWidgetFormFilterInput(array('with_empty' => false)),
      'frame_url'    => new sfWidgetFormFilterInput(array('with_empty' => false)),
      'credential'   => new sfWidgetFormFilterInput(array('with_empty' => false)),
    ));

    $this->setValidators(array(
      'name'         => new sfValidatorPass(array('required' => false)),
      'image'        => new sfValidatorPass(array('required' => false)),
      'subdomain'    => new sfValidatorPass(array('required' => false)),
      'informations' => new sfValidatorPass(array('required' => false)),
      'frame_url'    => new sfValidatorPass(array('required' => false)),
      'credential'   => new sfValidatorPass(array('required' => false)),
    ));

    $this->widgetSchema->setNameFormat('server_filters[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    $this->setupInheritance();

    parent::setup();
  }

  public function getModelName()
  {
    return 'Server';
  }

  public function getFields()
  {
    return array(
      'id'           => 'Number',
      'name'         => 'Text',
      'image'        => 'Text',
      'subdomain'    => 'Text',
      'informations' => 'Text',
      'frame_url'    => 'Text',
      'credential'   => 'Text',
    );
  }
}
