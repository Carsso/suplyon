<?php

/**
 * Member filter form.
 *
 * @package    SupLyon
 * @subpackage filter
 * @author     Your name here
 * @version    SVN: $Id: sfDoctrineFormFilterTemplate.php 23810 2009-11-12 11:07:44Z Kris.Wallsmith $
 */
class MemberFormFilter extends BaseMemberFormFilter
{
  public function configure()
  {
  	unset(
  		$this['type'],
  		$this['pseudo'],
  		$this['role'],
  		$this['rank'],
  		$this['phone'],
  		$this['site'],
  		$this['facebook'],
  		$this['twitter'],
  		$this['skype'],
  		$this['windowslive'],
  		$this['ejabberd_password'],
  		$this['created_at'],
  		$this['updated_at'],
  		$this['slug']
  	);
  }
}
