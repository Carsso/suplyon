<?php

/**
 * sfGuardUserProfile filter form.
 *
 * @package    SupLyon
 * @subpackage filter
 * @author     Your name here
 * @version    SVN: $Id: sfDoctrinePluginFormFilterTemplate.php 23810 2009-11-12 11:07:44Z Kris.Wallsmith $
 */
class sfGuardUserProfileFormFilter extends PluginsfGuardUserProfileFormFilter
{
  public function configure()
  {
  	unset($this['first_name'],$this['last_name']);
  }
}
