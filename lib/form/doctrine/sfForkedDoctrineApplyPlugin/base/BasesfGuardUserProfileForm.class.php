<?php

/**
 * sfGuardUserProfile form base class.
 *
 * @method sfGuardUserProfile getObject() Returns the current form's model object
 *
 * @package    SupLyon
 * @subpackage form
 * @author     Your name here
 * @version    SVN: $Id: sfDoctrineFormGeneratedTemplate.php 29553 2010-05-20 14:33:00Z Kris.Wallsmith $
 */
abstract class BasesfGuardUserProfileForm extends BaseFormDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'user_id'           => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('User'), 'add_empty' => false)),
      'type'              => new sfWidgetFormInputText(),
      'idbooster'         => new sfWidgetFormInputHidden(),
      'pseudo'            => new sfWidgetFormInputText(),
      'name'              => new sfWidgetFormInputText(),
      'role'              => new sfWidgetFormInputText(),
      'campusid'          => new sfWidgetFormInputText(),
      'campus'            => new sfWidgetFormInputText(),
      'class'             => new sfWidgetFormInputText(),
      'cursus'            => new sfWidgetFormInputText(),
      'promotion'         => new sfWidgetFormInputText(),
      'rank'              => new sfWidgetFormInputText(),
      'phone'             => new sfWidgetFormInputText(),
      'site'              => new sfWidgetFormInputText(),
      'facebook'          => new sfWidgetFormInputText(),
      'twitter'           => new sfWidgetFormInputText(),
      'skype'             => new sfWidgetFormInputText(),
      'windowslive'       => new sfWidgetFormInputText(),
      'last_connexion'    => new sfWidgetFormDateTime(),
      'ejabberd_password' => new sfWidgetFormInputText(),
      'created_at'        => new sfWidgetFormDateTime(),
      'updated_at'        => new sfWidgetFormDateTime(),
      'slug'              => new sfWidgetFormInputText(),
    ));

    $this->setValidators(array(
      'user_id'           => new sfValidatorDoctrineChoice(array('model' => $this->getRelatedModelName('User'))),
      'type'              => new sfValidatorString(array('max_length' => 255, 'required' => false)),
      'idbooster'         => new sfValidatorChoice(array('choices' => array($this->getObject()->get('idbooster')), 'empty_value' => $this->getObject()->get('idbooster'), 'required' => false)),
      'pseudo'            => new sfValidatorString(array('max_length' => 255, 'required' => false)),
      'name'              => new sfValidatorString(array('max_length' => 255, 'required' => false)),
      'role'              => new sfValidatorString(array('max_length' => 45, 'required' => false)),
      'campusid'          => new sfValidatorInteger(array('required' => false)),
      'campus'            => new sfValidatorString(array('max_length' => 255, 'required' => false)),
      'class'             => new sfValidatorString(array('max_length' => 30, 'required' => false)),
      'cursus'            => new sfValidatorString(array('max_length' => 45, 'required' => false)),
      'promotion'         => new sfValidatorString(array('max_length' => 30, 'required' => false)),
      'rank'              => new sfValidatorString(array('max_length' => 45, 'required' => false)),
      'phone'             => new sfValidatorString(array('max_length' => 45, 'required' => false)),
      'site'              => new sfValidatorString(array('max_length' => 255, 'required' => false)),
      'facebook'          => new sfValidatorString(array('max_length' => 255, 'required' => false)),
      'twitter'           => new sfValidatorString(array('max_length' => 255, 'required' => false)),
      'skype'             => new sfValidatorString(array('max_length' => 255, 'required' => false)),
      'windowslive'       => new sfValidatorString(array('max_length' => 255, 'required' => false)),
      'last_connexion'    => new sfValidatorDateTime(array('required' => false)),
      'ejabberd_password' => new sfValidatorString(array('max_length' => 255, 'required' => false)),
      'created_at'        => new sfValidatorDateTime(),
      'updated_at'        => new sfValidatorDateTime(),
      'slug'              => new sfValidatorString(array('max_length' => 255, 'required' => false)),
    ));

    $this->validatorSchema->setPostValidator(
      new sfValidatorAnd(array(
        new sfValidatorDoctrineUnique(array('model' => 'sfGuardUserProfile', 'column' => array('user_id'))),
        new sfValidatorDoctrineUnique(array('model' => 'sfGuardUserProfile', 'column' => array('slug'))),
      ))
    );

    $this->widgetSchema->setNameFormat('sf_guard_user_profile[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    $this->setupInheritance();

    parent::setup();
  }

  public function getModelName()
  {
    return 'sfGuardUserProfile';
  }

}
