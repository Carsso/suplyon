<?php

/**
 * Ordered form base class.
 *
 * @method Ordered getObject() Returns the current form's model object
 *
 * @package    SupLyon
 * @subpackage form
 * @author     Your name here
 * @version    SVN: $Id: sfDoctrineFormGeneratedTemplate.php 29553 2010-05-20 14:33:00Z Kris.Wallsmith $
 */
abstract class BaseOrderedForm extends BaseFormDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'id'           => new sfWidgetFormInputHidden(),
      'pizza_id'     => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('Pizza'), 'add_empty' => false)),
      'idbooster'    => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('Member'), 'add_empty' => false)),
      'transaction'  => new sfWidgetFormInputText(),
      'email_paypal' => new sfWidgetFormInputText(),
      'payed'        => new sfWidgetFormInputCheckbox(),
      'created_at'   => new sfWidgetFormDateTime(),
      'updated_at'   => new sfWidgetFormDateTime(),
    ));

    $this->setValidators(array(
      'id'           => new sfValidatorChoice(array('choices' => array($this->getObject()->get('id')), 'empty_value' => $this->getObject()->get('id'), 'required' => false)),
      'pizza_id'     => new sfValidatorDoctrineChoice(array('model' => $this->getRelatedModelName('Pizza'))),
      'idbooster'    => new sfValidatorDoctrineChoice(array('model' => $this->getRelatedModelName('Member'))),
      'transaction'  => new sfValidatorString(array('max_length' => 255, 'required' => false)),
      'email_paypal' => new sfValidatorString(array('max_length' => 255, 'required' => false)),
      'payed'        => new sfValidatorBoolean(array('required' => false)),
      'created_at'   => new sfValidatorDateTime(),
      'updated_at'   => new sfValidatorDateTime(),
    ));

    $this->widgetSchema->setNameFormat('ordered[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    $this->setupInheritance();

    parent::setup();
  }

  public function getModelName()
  {
    return 'Ordered';
  }

}
