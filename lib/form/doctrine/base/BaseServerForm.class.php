<?php

/**
 * Server form base class.
 *
 * @method Server getObject() Returns the current form's model object
 *
 * @package    SupLyon
 * @subpackage form
 * @author     Your name here
 * @version    SVN: $Id: sfDoctrineFormGeneratedTemplate.php 29553 2010-05-20 14:33:00Z Kris.Wallsmith $
 */
abstract class BaseServerForm extends BaseFormDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'id'           => new sfWidgetFormInputHidden(),
      'name'         => new sfWidgetFormInputText(),
      'image'        => new sfWidgetFormInputText(),
      'subdomain'    => new sfWidgetFormInputText(),
      'informations' => new sfWidgetFormInputText(),
      'frame_url'    => new sfWidgetFormInputText(),
      'credential'   => new sfWidgetFormInputText(),
    ));

    $this->setValidators(array(
      'id'           => new sfValidatorChoice(array('choices' => array($this->getObject()->get('id')), 'empty_value' => $this->getObject()->get('id'), 'required' => false)),
      'name'         => new sfValidatorString(array('max_length' => 45)),
      'image'        => new sfValidatorString(array('max_length' => 255)),
      'subdomain'    => new sfValidatorString(array('max_length' => 45)),
      'informations' => new sfValidatorPass(),
      'frame_url'    => new sfValidatorString(array('max_length' => 255)),
      'credential'   => new sfValidatorString(array('max_length' => 45)),
    ));

    $this->widgetSchema->setNameFormat('server[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    $this->setupInheritance();

    parent::setup();
  }

  public function getModelName()
  {
    return 'Server';
  }

}
