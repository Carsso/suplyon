<?php

/**
 * Order form base class.
 *
 * @method Order getObject() Returns the current form's model object
 *
 * @package    SupLyon
 * @subpackage form
 * @author     Your name here
 * @version    SVN: $Id: sfDoctrineFormGeneratedTemplate.php 29553 2010-05-20 14:33:00Z Kris.Wallsmith $
 */
abstract class BaseOrderForm extends BaseFormDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'id'              => new sfWidgetFormInputHidden(),
      'idbooster'       => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('Member'), 'add_empty' => false)),
      'is_payed'        => new sfWidgetFormInputCheckbox(),
      'payment_type'    => new sfWidgetFormInputText(),
      'transaction_ref' => new sfWidgetFormInputText(),
      'email_paypal'    => new sfWidgetFormInputText(),
      'status'          => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('OrderStatuses'), 'add_empty' => false)),
      'shipping_price'  => new sfWidgetFormInputText(),
      'extra_price'     => new sfWidgetFormInputText(),
      'created_at'      => new sfWidgetFormDateTime(),
      'updated_at'      => new sfWidgetFormDateTime(),
    ));

    $this->setValidators(array(
      'id'              => new sfValidatorChoice(array('choices' => array($this->getObject()->get('id')), 'empty_value' => $this->getObject()->get('id'), 'required' => false)),
      'idbooster'       => new sfValidatorDoctrineChoice(array('model' => $this->getRelatedModelName('Member'))),
      'is_payed'        => new sfValidatorBoolean(array('required' => false)),
      'payment_type'    => new sfValidatorString(array('max_length' => 255)),
      'transaction_ref' => new sfValidatorString(array('max_length' => 255, 'required' => false)),
      'email_paypal'    => new sfValidatorString(array('max_length' => 255, 'required' => false)),
      'status'          => new sfValidatorDoctrineChoice(array('model' => $this->getRelatedModelName('OrderStatuses'))),
      'shipping_price'  => new sfValidatorPass(array('required' => false)),
      'extra_price'     => new sfValidatorPass(array('required' => false)),
      'created_at'      => new sfValidatorDateTime(),
      'updated_at'      => new sfValidatorDateTime(),
    ));

    $this->widgetSchema->setNameFormat('order[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    $this->setupInheritance();

    parent::setup();
  }

  public function getModelName()
  {
    return 'Order';
  }

}
