<?php

/**
 * Pizza form base class.
 *
 * @method Pizza getObject() Returns the current form's model object
 *
 * @package    SupLyon
 * @subpackage form
 * @author     Your name here
 * @version    SVN: $Id: sfDoctrineFormGeneratedTemplate.php 29553 2010-05-20 14:33:00Z Kris.Wallsmith $
 */
abstract class BasePizzaForm extends BaseFormDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'id'             => new sfWidgetFormInputHidden(),
      'name'           => new sfWidgetFormInputText(),
      'ingredients'    => new sfWidgetFormInputText(),
      'dominos_id'     => new sfWidgetFormInputText(),
      'price'          => new sfWidgetFormInputText(),
      'original_price' => new sfWidgetFormInputText(),
      'is_visible'     => new sfWidgetFormInputCheckbox(),
      'is_special'     => new sfWidgetFormInputCheckbox(),
      'slug'           => new sfWidgetFormInputText(),
    ));

    $this->setValidators(array(
      'id'             => new sfValidatorChoice(array('choices' => array($this->getObject()->get('id')), 'empty_value' => $this->getObject()->get('id'), 'required' => false)),
      'name'           => new sfValidatorString(array('max_length' => 45)),
      'ingredients'    => new sfValidatorPass(),
      'dominos_id'     => new sfValidatorInteger(array('required' => false)),
      'price'          => new sfValidatorPass(array('required' => false)),
      'original_price' => new sfValidatorPass(array('required' => false)),
      'is_visible'     => new sfValidatorBoolean(array('required' => false)),
      'is_special'     => new sfValidatorBoolean(array('required' => false)),
      'slug'           => new sfValidatorString(array('max_length' => 255, 'required' => false)),
    ));

    $this->validatorSchema->setPostValidator(
      new sfValidatorDoctrineUnique(array('model' => 'Pizza', 'column' => array('slug')))
    );

    $this->widgetSchema->setNameFormat('pizza[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    $this->setupInheritance();

    parent::setup();
  }

  public function getModelName()
  {
    return 'Pizza';
  }

}
