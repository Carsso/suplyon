<?php

class dominospizzaTask extends sfBaseTask
{
    protected function configure()
    {
        // // add your own arguments here
        // $this->addArguments(array(
        //   new sfCommandArgument('my_arg', sfCommandArgument::REQUIRED, 'My argument'),
        // ));

        $this->addOptions(array(
            new sfCommandOption('application', null, sfCommandOption::PARAMETER_REQUIRED, 'The application name'),
            new sfCommandOption('env', null, sfCommandOption::PARAMETER_REQUIRED, 'The environment', 'dev'),
            new sfCommandOption('connection', null, sfCommandOption::PARAMETER_REQUIRED, 'The connection name', 'doctrine'),
            // add your own options here
        ));

        $this->namespace = 'dominos';
        $this->name = 'update-pizzas';
        $this->briefDescription = '';
        $this->detailedDescription = <<<EOF
The [dominospizza|INFO] task does things.
Call it with:

  [php symfony dominospizza|INFO]
EOF;
    }

    protected function execute($arguments = array(), $options = array())
    {
        set_include_path(sfConfig::get('sf_lib_dir') . '/external' . PATH_SEPARATOR . get_include_path());
        require_once sfConfig::get('sf_lib_dir') . '/external/simple_html_dom.php';
        // initialize the database connection
        $databaseManager = new sfDatabaseManager($this->configuration);
        $connection = $databaseManager->getDatabase($options['connection'])->getConnection();
        $d_pizzas = Doctrine_Core::getTable('Pizza')->findAll(Doctrine::HYDRATE_ARRAY);
        $array_pizza = array();
        foreach ($d_pizzas as $pizza) {
            $array_pizza[$pizza['id']] = $pizza;
            $array_pizza[$pizza['id']]['is_visible'] = 0;
        }
        // Create DOM from URL or file
        $html = file_get_html('http://mobile.dominos.fr/lacarte/lacarte.php?id_mag=31740&page=pizzas');
        $table = $html->find('table', 0);
        foreach ($table->find('tr') as $tr) {
            $dominos_id = str_replace(array('img/pizzas/', '_light.gif'), array('', ''), $tr->find('.colone_img a img', 0)->src);
            $name = str_replace('*', '', $tr->find('.colone span.pizza', 0)->plaintext);
            $ingredients = $tr->find('.colone span.info', 0)->plaintext;
            $original_price = (double)str_replace('€', '', $tr->find('.colone span.prix strong', 0)->plaintext);
            $array_pizza[$dominos_id]['dominos_id'] = $dominos_id;
            $array_pizza[$dominos_id]['name'] = html_entity_decode($name);
            $array_pizza[$dominos_id]['ingredients'] = html_entity_decode($ingredients);
            $array_pizza[$dominos_id]['original_price'] = $original_price;
            $array_pizza[$dominos_id]['price'] = 8;
            $array_pizza[$dominos_id]['is_visible'] = 1;
        }
        foreach ($array_pizza as $id => $pizz) {
            $pizza = Doctrine_Core::getTable('Pizza')->find($id);
            if (!$pizza) {
                $pizza = new Pizza();
            }
            $pizza->fromArray($pizz);
            $pizza->setId($id);
            $pizza->save();
        }


        // add your code here
    }
}
