<?php use_helper('I18N') ?>
<?php use_stylesheets_for_form( $form ) ?>
<?php
  // Override the login slot so that we don't get a login prompt on the
  // apply page, which is just odd-looking. 0.6
?>
<?php slot('sf_apply_login') ?>
<?php end_slot() ?>

<?php slot('rightcolumn') ?>
  <?php include_partial('global/inscrrightcolumn') ?>
<?php end_slot() ?>
<div class="sf_apply sf_apply_apply estateitem bordered shadowed rounded text_center">
<h4><?php echo __("Apply for an Account", array(), 'sfForkedApply') ?></h4>
<?php if($hideJainRain) { ?>
<h5 style="margin-top:10px;">Compte enregistré, merci de remplir le formulaire pour finaliser votre inscription.</h5>
<?php } else { ?>
  <h5 style="margin-top:10px;">Utilisez votre compte:</h5>
  <p style="margin-top:0px">
    <a class="rpxnow" onclick="return false;"
    href="https://immowwi.rpxnow.com/openid/v2/signin?token_url=<?php echo url_for1('jainrain_login', true) ?>">
      <?php echo image_tag('j_facebook.png', array('onclick'=>'RPXNOW.default_provider = "facebook";', 'alt'=>'Connexion avec Facebook')) ?>
      <?php echo image_tag('j_twitter.png', array('onclick'=>'RPXNOW.default_provider = "twitter";', 'alt'=>'Connexion avec Twitter')) ?> 
      <?php echo image_tag('j_google.png', array('onclick'=>'RPXNOW.default_provider = "google";', 'alt'=>'Connexion avec Google')) ?>
      <?php echo image_tag('j_wlm.png', array('onclick'=>'RPXNOW.default_provider = "live_id";', 'alt'=>'Connexion avec Live')) ?> 
      <?php echo image_tag('j_yahoo.png', array('onclick'=>'RPXNOW.default_provider = "yahoo";', 'alt'=>'Connexion avec Yahoo')) ?>
      <?php echo image_tag('j_openid.png', array('onclick'=>'RPXNOW.default_provider = "openid";', 'alt'=>'Connexion avec OpenId')) ?>  
    </a>
  </p>
<?php } ?>
<form method="post" action="<?php echo url_for('sfApply/apply') ?>" name="sf_apply_apply_form" id="sf_apply_apply_form">
  <?php echo $form['firstname']->renderError() ?>
  <p>
    <label class="hidden" for="sfApplyApply_firstname">Prénom</label>
    <?php echo $form['firstname'] ?>
  </p>
  <?php echo $form['lastname']->renderError() ?>
  <p>
    <label class="hidden" for="sfApplyApply_lastname">Nom</label> <?php echo $form['lastname'] ?>
  </p>
  <?php echo $form['email']->renderError() ?>
  <p>
    <label class="hidden" for="sfApplyApply_email">Email</label> <?php echo $form['email'] ?>
  </p>
  <?php echo $form['password']->getError() ?>
  <p>
    <label class="hidden" for="sfApplyApply_password">Mot de passe</label> <?php echo $form['password'] ?>
  </p>
  <?php echo $form['password2']->renderError() ?>
  <p>
    <label class="hidden" for="sfApplyApply_password2">Confirmer le mot de passe</label> <?php echo $form['password2'] ?>
  </p>
  <?php echo $form['captcha']->renderError() ?>
  <?php echo $form['captcha'] ?>
  <div class="options_insc">
    <div class="label_opt_insc">Je souhaite recevoir la newsletter d'Immowwi</div>
    <div class="input_opt_insc text_left"><?php echo $form['opt_in'] ?></div>
  </div>
  <div class="options_insc">
    <div class="label_opt_insc">Je souhaite recevoir les offres des partenaires d'Immowwi</div>
    <div class="input_opt_insc text_left"><?php echo $form['offers'] ?></div>
  </div>
  <?php echo $form->renderHiddenFields(); ?>
  <?php echo $form['cgu']->renderError() ?>
  <div class="options_insc">
    <div class="label_opt_insc">J'accepte les conditions générales d'utilisation et je les accepte sans réserve</div>
    <div class="input_opt_insc_2 text_left"><?php echo $form['cgu'] ?></div>
  </div>
  <p>
    <input type="submit" class="designedbutton" value="<?php echo __("Create My Account", array(), 'sfForkedApply') ?>" />
  </p>
</form>
</div>
