<?php use_helper('I18N') ?>
<h4>Connexion</h4>
<form method="post" action="<?php echo url_for("@sf_guard_signin") ?>" name="sf_guard_signin" id="sf_guard_signin" class="sf_apply_signin_inline">
  <p style="margin-top:0px">
    <label class="hidden" for="signin_username">Adresse Email</label> <?php echo $form['username']->render(array('autocomplete'=>'off')) ?>
  </p>
  <p>
    <label class="hidden" for="signin_password">Mot de passe</label> <?php echo $form['password'] ?>
  </p>
  <p>
    <?php echo $form->renderHiddenFields() ?>
    <input type="submit" value="<?php echo __('sign in', array(), 'sfForkedApply') ?>" class="designedbutton" style="float:right;margin-right:12%;"/>
  </p>
  <div class="clear"></div>
  <p>
  <?php echo link_to(__('Reset Your Password',array(),'sfForkedApply'), 'sfApply/resetRequest')  ?>
  </p>
  <p>
  <?php
  echo link_to(__('Create a New Account',array(),'sfForkedApply'), 'sfApply/apply')
  ?>
  </p>
</form>
<script type="text/javascript">
  $(window).load(function(){
    if($('.rightcolumn #signin_username').val() != ""){
      $('.rightcolumn #spanLabel0').css('display','none');
    }
    if($('.rightcolumn #signin_password').val() != ""){
      $('.rightcolumn #spanLabel1').css('display','none');
    }
  });
</script>