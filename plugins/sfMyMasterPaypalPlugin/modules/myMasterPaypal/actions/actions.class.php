<?php

/**
 * commande actions.
 *
 * @package    name
 * @subpackage commande
 * @author     Your name here
 * @version    SVN: $Id: actions.class.php 23810 2009-11-12 11:07:44Z Kris.Wallsmith $
 */
class myMasterPaypalActions extends sfActions
{
	public function executeDone(sfWebRequest $request)
	{
		$paypal = new sfMyMasterPaypal('8.60', 'Paiement d\'une Pizza - Prix: 8.60€');
		$result = $paypal->doPayment($request);
		if($result){
			$paypal->unsetParameters(array('PAYMENTACTION','PAYERID'));
			$details = $paypal->getPaymentDetails($request);
		    $order_info = explode('-',$details['CUSTOM']);
			$ordered = new Ordered();
			$ordered->setPizzaId($order_info[1])
			->setIdbooster($order_info[0])
			->setTransaction('Paypal - '.$result['TRANSACTIONTYPE'].' - '.$result['ORDERTIME'].' - '.$result['AMT'].' - '.$result['TRANSACTIONID'])
			->setEmailPaypal($details['EMAIL'])
			->setPayed(1)
			->save();
			
			$admins = Doctrine_Query::create()->select('email_address ')->from('sfGuardUser')->where('is_super_admin = 1')->execute(array(), Doctrine::HYDRATE_SINGLE_SCALAR);
			$destinataires = array($ordered->getMember()->getUser()->getEmailAddress()=>$ordered->getMember()->getName());
			$subject = '[Pizza] Confirmation de transaction';
			$content = '<img src="https://suplyon.fr/images/maillogo.png" alt="SupLyon"><br>';
			$content .= 'Bonjour '.$ordered->getMember()->getName().',<br>';
			$content .= 'Tu viens de commander une pizza "'.$ordered->getPizzaName().'" sur SupLyon.fr via PayPal avec l\'adresse email suivante: "'.$details['EMAIL'].'"<br>';
			$content .= 'Voici un réapitulatif de cette transaction :<br>';
			$content .= ' - Adresse Email utilisée : '.$details['EMAIL'].'<br>';
			$content .= ' - Type de transaction : PayPal '.$result['TRANSACTIONTYPE'].'<br>';
			$content .= ' - Référence transaction : '.$result['TRANSACTIONID'].'<br>';
			$content .= ' - Heure de transaction : '.$result['ORDERTIME'].'<br>';
			$content .= ' - Montant de la transaction : '.$result['AMT'].'€'.'<br>';
			$content .= '<br>';
			$content .= 'Cordialement,<br>';
			$content .= 'L\'Equipe SupLyon.fr';
			$expediteur = array('contact@suplyon.fr'=>'SupLyon.fr');
	  		$context = sfContext::getInstance();
	  		$message = $context->getMailer()->compose($expediteur,
	  		$destinataires,
	  		'[SupLyon] '.$subject,
	  		$content);
	  		$message = $message->setReplyTo($request->getParameter('from',null),$request->getParameter('name',null));
	  		$message->setContentType("text/html");
	  		$context->getMailer()->send($message);
			$this->redirect(url_for('@pizza').'?payment=ok');
		} else {
			die('An unknown error has occured, please contact your administrator !');
		}
	}
	public function executeCancel(sfWebRequest $request)
	{
		$paypal = new sfMyMasterPaypal('8.60', 'Paiement d\'une Pizza - Prix: 8.60€');
		$details = $paypal->getPaymentDetails($request);
		$this->redirect(url_for('@pizza').'?payment=error');
	}
}
